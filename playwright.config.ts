import { PlaywrightTestConfig } from '@playwright/test';
import { globalPaths, outputDirs, storageDir } from '@test.setup';
import * as process from "process";

const config: PlaywrightTestConfig = {
    testDir: './playwright-tests',
    timeout: 30_000,
    expect: { timeout: 5000 },
    fullyParallel: true,
    forbidOnly: !!process.env.CI,
    reporter: [
        [process.env.CI ? 'github': 'list'],
        ['html', { open: 'never', outputFolder: outputDirs.htmlOutputDir }]
    ],
    use: {
        actionTimeout: 15_000,
        storageState: storageDir.storageState,
        baseURL: 'https://automationexercise.com',
        ignoreHTTPSErrors: true,
        trace: 'retain-on-failure',
    },
    projects: [
        {
            name: 'Chrome',
            use: {
                browserName: 'chromium',
                channel: 'chrome',
                viewport: {
                    width: 1920,
                    height: 1080,
                },
            },
        },
        {
            name: 'Webkit',
            use: {
                browserName: 'webkit',
                viewport: {
                    width: 1920,
                    height: 1080,
                },
            },
            dependencies: ['Chrome']
        },
    ],
    globalSetup: globalPaths.apiGlobalSetup,
    outputDir: 'test-results/',
};

export default config;
