import { test } from 'playwright-elements';
import { CheckoutPage, MainPage, PaymentDonePage, PaymentsPage, ProductsPage, ViewCartPage } from '@page.object';

const productsPage = new ProductsPage();
const viewCartPage = new ViewCartPage();
const checkoutPage = new CheckoutPage();
const paymentsPage = new PaymentsPage();
const paymentDonePage = new PaymentDonePage();

test.beforeEach(async ({ goto }) => {
    await test.step(`Navigate to https://automationexercise.com/`, async () => {
        await goto('/');
    });
})

test('Order could be placed', async () => {
    const productName = 'Sleeveless Dress'
    await test.step(`Click the “Products” button.`, async () => {
        const mainPage = new MainPage();
        await mainPage.navBar.clickOnProductsBtn();
    });

    await test.step(`Search for “Sleeveless Dress”`, async () => {
        await productsPage.searchProduct(productName);
    });

    await test.step(`Assert that the first search result contains the correct search term.`, async () => {
        await productsPage.verifyFirstProductName(productName);
    });

    await test.step(`Add the first product to cart.`, async () => {
        await productsPage.addFirstProductToCart();
    });

    await test.step(`Click the “View Cart” link.`, async () => {
        await productsPage.modal.viewCart();
    });

    await test.step(`Assert that the correct product is added to the cart.`, async () => {
        await viewCartPage.verifyProductPresentByName(productName);
    });

    await test.step(`Click the “Proceed To Checkout” button.`, async () => {
        await viewCartPage.clickProceedToCheckoutBtn();
    });

    await test.step(`Click the “Place Order” button.`, async () => {
        await checkoutPage.clickPlaceOrderBtn();
    });

    await test.step(`Enter card details.`, async () => {
        await paymentsPage.enterCardDetails();
        await paymentsPage.submitOrder();
    });

    await test.step(`Assert that the order has been successfully placed.`, async () => {
        await paymentDonePage.verifyOrderPlaced();
    });
});
