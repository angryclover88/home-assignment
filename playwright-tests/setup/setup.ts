import { APIRequestContext, FullConfig, request } from '@playwright/test';
import { storageDir, userCredentials } from '@test.setup';

export default async function globalSetup(config: FullConfig) {
    const { baseURL } = config.projects[0].use;
    const loginEndpoint = '/login';
    const context = await request.newContext({
        baseURL: `${baseURL}`,
    });
    try {
        const token = await getCsrfToken(context)
        await context.post(loginEndpoint, {
            headers: {
                referer: baseURL + loginEndpoint,
            },
            form: {
                csrfmiddlewaretoken: token,
                email: userCredentials.email,
                password: userCredentials.password,
            }
        });
    } catch (error) {
        throw new Error(`${(error as Error).message}`);
    }
    await context.storageState({ path: storageDir.storageState });
}

async function getCsrfToken(context: APIRequestContext): Promise<string> {
    const res = await context.get('/');
    return /<input[^>]*name="csrfmiddlewaretoken"[^>]*value="([^"]*)"/.exec(await res.text())?.[1] ?? '';
}