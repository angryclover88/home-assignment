export const storageDir = {
    storageState: 'playwright-tests/storage-state/state.json',
};

export const globalPaths = {
    apiGlobalSetup: 'playwright-tests/setup/setup.ts',
};

export const userCredentials = {
    email: 'angryclover88@gmail.com',
    password: 'Password123',
}

export const outputDirs = {
    htmlOutputDir: './playwright-report'
}