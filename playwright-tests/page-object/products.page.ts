import { Modal } from '@page.components';
import { $ } from 'playwright-elements';

export class ProductsPage {
    readonly modal = new Modal()
    private readonly searchInput = $('#search_product');
    private readonly searchBtn = $('#submit_search');
    private readonly product = $('.product-image-wrapper').subElements({
            productName: $('.productinfo p'),
            addToCartBtn: $('.overlay-content .add-to-cart')
        }
    )

    async searchProduct(searchTerm: string): Promise<void> {
        await this.searchInput.clear();
        await this.searchInput.fill(searchTerm);
        await this.searchBtn.click();
    }

    async verifyFirstProductName(productName: string): Promise<void> {
        await this.product.first().productName
            .expect(`Product name should be '${productName}'`)
            .toHaveText(productName);
    }

    async addFirstProductToCart(): Promise<void> {
        await this.product.productName.hover();
        await this.product.addToCartBtn.click();
    }
}
