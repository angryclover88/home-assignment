import { $, expect } from 'playwright-elements';

export class ViewCartPage {
    private readonly products = $('[id*=product]');
    private readonly proceedToCheckoutBtn = $('a.check_out');

    async verifyProductPresentByName(productName: string): Promise<void> {
        const prods = await this.products.filter(async product =>
            await product.$('.cart_description h4').hasText(productName).isVisible()
        )
        expect(prods.length, `${productName} should be present in cart`).toBe(1);
    }

    async clickProceedToCheckoutBtn(): Promise<void> {
        await this.proceedToCheckoutBtn.click();
    }
}