import { $ } from 'playwright-elements';

export class PaymentDonePage {
    private readonly header = $(`[data-qa='order-placed']:visible`)

    async verifyOrderPlaced(): Promise<void> {
        await this.header.expect('Order should be placed').toHaveText('Order Placed!');
    }
}