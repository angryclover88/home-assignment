import { $ } from 'playwright-elements';

export class Modal {
    private readonly modal = $('.modal-content').subElements({
        viewCartLink: $(`[href='/view_cart']`)
    });

    async viewCart(): Promise<void> {
        await this.modal.viewCartLink.click();
    }
}