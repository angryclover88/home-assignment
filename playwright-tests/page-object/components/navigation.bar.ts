import { $, BrowserInstance } from 'playwright-elements';

export class NavigationBar {
    private readonly navBar = $('.navbar-nav').subElements({
        productsBtn: $(`[href='/products']`),
    });

    async clickOnProductsBtn(): Promise<void> {
        await this.navBar.productsBtn.click();
        await BrowserInstance.currentPage.goBack();
        await this.navBar.productsBtn.click();
    }
}
