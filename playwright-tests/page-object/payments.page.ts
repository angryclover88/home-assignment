import { $ } from 'playwright-elements';
import { faker } from '@faker-js/faker';

export class PaymentsPage {
    private readonly nameOnCardInput = $(`[name='name_on_card']`)
    private readonly cardNumber = $(`.card-number`);
    private readonly cvc = $(`.card-cvc`);
    private readonly expirationMonth = $(`.card-expiry-month`);
    private readonly expirationYear = $(`.card-expiry-year`);
    private readonly submit = $(`#submit`);

    async enterCardDetails() {
        await this.nameOnCardInput.fill(faker.name.fullName());
        await this.cardNumber.fill(faker.finance.creditCardNumber());
        await this.cvc.fill(faker.finance.creditCardCVV());
        await this.expirationMonth.fill(faker.date.future(1).getMonth().toString());
        await this.expirationYear.fill(faker.date.future(1, ).getFullYear().toString());
    }

    async submitOrder() {
        await this.submit.click();
    }
}