import { $ } from 'playwright-elements';

export class CheckoutPage {
    private readonly placeOrderBtn = $(`[href='/payment']`);

    async clickPlaceOrderBtn(): Promise<void> {
        await this.placeOrderBtn.click();
    }
}