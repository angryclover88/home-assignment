import { NavigationBar } from '@page.components';

export class MainPage {
    readonly navBar = new NavigationBar();
}
