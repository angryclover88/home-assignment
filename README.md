# home-assignment

## Getting started

Install dev dependencies:

```
npm install
```

Launch tests:
```
npx playwright test --config=playwright.config.ts
```

Get report:
```
 npx playwright show-report
```
